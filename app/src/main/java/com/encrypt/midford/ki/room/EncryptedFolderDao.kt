package com.encrypt.midford.ki.room

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface EncryptedFolderDao {

    @Query("SELECT * FROM folders")
    fun getAllFolders(): LiveData<List<EncryptedFolder>?>

    @Query("SELECT * FROM folders WHERE parent IS NULL")
    fun getRootFolders(): LiveData<List<EncryptedFolder>?>

    @Query("SELECT * FROM folders WHERE parent=:parentId")
    fun getFolders(parentId: Long?): LiveData<List<EncryptedFolder>?>

    @Query("SELECT * FROM folders WHERE id=:id LIMIT 1")
    fun getFolder(id: Long?): LiveData<EncryptedFolder?>

    @Query("SELECT * FROM folders WHERE id=:id LIMIT 1")
    fun getFolderSync(id: Long?): EncryptedFolder?

    @Query("SELECT * FROM folders WHERE parent=:parentId")
    fun getFoldersSync(parentId: Long?): List<EncryptedFolder>?

    @Insert
    fun insertFolder(encryptedFolder: EncryptedFolder): Long

    @Update
    fun updateFolder(encryptedFolder: EncryptedFolder)

    @Delete
    fun deleteFolder(encryptedFolder: EncryptedFolder)
}
