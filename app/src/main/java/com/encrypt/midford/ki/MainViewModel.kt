package com.encrypt.midford.ki

import android.app.Application
import android.content.ContentResolver
import android.net.Uri
import androidx.lifecycle.*

import com.encrypt.midford.ki.encrypt.EncryptException
import com.encrypt.midford.ki.encrypt.Encryptor

import com.encrypt.midford.ki.data.AppRepository
import com.encrypt.midford.ki.room.EncryptedFile
import com.encrypt.midford.ki.room.EncryptedFolder
import com.encrypt.midford.ki.utils.FileType
import com.encrypt.midford.ki.utils.Utils

import java.io.InputStream
import java.util.*

internal class MainViewModel(
          app: Application
        , private val decryptor: Encryptor
) : AndroidViewModel(app) {

    private val mRepository: AppRepository = AppRepository(app, decryptor)

    var isSearchMode: Boolean = false

    val isLoading: LiveData<Boolean>
        get() {
            val result = MediatorLiveData<Boolean>()

            result.addSource(isFoldersLoading) {
                result.value = it && isFilesLoading.value ?: false
            }

            result.addSource(isFilesLoading) {
                result.value = it && isFoldersLoading.value ?: false
            }

            return result
        }

    private val isFoldersLoading: MutableLiveData<Boolean> = MutableLiveData()
    private val isFilesLoading: MutableLiveData<Boolean> = MutableLiveData()

    private val mCurFolderId: MutableLiveData<Long?> = MutableLiveData()
    private var mCurFolder: LiveData<EncryptedFolder?>
            = Transformations.switchMap(mCurFolderId) { id -> mRepository.getEncryptedFolder(id) }

    val curFolderName: LiveData<String?>
            = Transformations.map(mCurFolder) { folder ->
        if (folder == null)
            null
        else
            String(decryptor.decrypt(folder.Name))
    }

    val isRootFolder: LiveData<Boolean>
            = Transformations.map(mCurFolder) { folder -> folder == null }

    private val mFolders: LiveData<List<EncryptedFolder>?>
            = Transformations.switchMap(mCurFolder) { c -> mRepository.getEncryptedFolders(c) }
    private val mFiles: LiveData<List<EncryptedFile>?>
            = Transformations.switchMap(mCurFolder) { c -> mRepository.getEncryptedFiles(c) }

    private val mFolderListItems: LiveData<List<MainViewModelListItem>?>
            = Transformations.map(mFolders) { input ->
        val result = ArrayList<MainViewModelListItem>()

        for (i in input!!)
            result.add(encryptedFolderToListItem(i))

        result
    }

    private val mFileListItems: LiveData<List<MainViewModelListItem>?>
            = Transformations.map(mFiles) { input ->
        val result = ArrayList<MainViewModelListItem>()

        for (i in input!!)
            result.add(encryptedFileToListItem(i))

        result
    }

    private val listItems: LiveData<List<MainViewModelListItem>>
        get() {
            val result = MediatorLiveData<List<MainViewModelListItem>>()

            result.addSource(mFileListItems) { items ->
                result.value = combineViewModelListItems( mFolderListItems.value, items )
                isFilesLoading.value = false
            }

            result.addSource(mFolderListItems) { items ->
                result.value = combineViewModelListItems( mFileListItems.value, items )
                isFoldersLoading.value = false
            }

            return result
        }

    private val searchQuery: MutableLiveData<String?> = MutableLiveData()

    private var showSearchResults: LiveData<Boolean>
            = Transformations.map(searchQuery) { it?.isNotEmpty() ?: false }

    private val mAllFolders: LiveData<List<EncryptedFolder>?>
            = mRepository.getAllEncryptedFolders()

    private val mAllFiles: LiveData<List<EncryptedFile>?>
            = mRepository.getAllEncryptedFiles()

    private val mFilterFolders: LiveData<List<EncryptedFolder>?>
        get() {
            val result = MediatorLiveData<List<EncryptedFolder>>()

            result.addSource(searchQuery) { query ->
                result.value = mAllFolders.value?.filter { i ->
                    Utils.isSubstring(String(decryptor.decrypt(i.Name)), query) }
            }

            result.addSource(mAllFolders) { items ->
                result.value = items?.filter { i ->
                    Utils.isSubstring(String(decryptor.decrypt(i.Name)), searchQuery.value) }
            }

            return result
        }

    private val mFilterFiles: LiveData<List<EncryptedFile>?>
        get() {
            val result = MediatorLiveData<List<EncryptedFile>>()

            result.addSource(searchQuery) { query ->
                result.value = mAllFiles.value?.filter { i ->
                    Utils.isSubstring(String(decryptor.decrypt(i.Name)), query) }
            }

            result.addSource(mAllFiles) { items ->
                result.value = items?.filter { i ->
                    Utils.isSubstring(String(decryptor.decrypt(i.Name)), searchQuery.value) }
            }

            return result
        }

    private val mTransformFolders: LiveData<List<MainViewModelListItem>>
            = Transformations.map(mFilterFolders) { input ->
        val result = ArrayList<MainViewModelListItem>()

        if (input != null)
            for (i in input)
                result.add(encryptedFolderToListItem(i))

        result
    }

    private val mTransformFiles: LiveData<List<MainViewModelListItem>>
            = Transformations.map(mFilterFiles) { input ->
        val result = ArrayList<MainViewModelListItem>()

        if (input != null)
            for (i in input)
                result.add(encryptedFileToListItem(i))

        result
    }

    private val searchResults: LiveData<List<MainViewModelListItem>>
        get() {
            val result = MediatorLiveData<List<MainViewModelListItem>>()

            result.addSource(mTransformFolders) { items ->
                result.value = combineViewModelListItems( mTransformFiles.value, items )
            }

            result.addSource(mTransformFiles) { items ->
                result.value = combineViewModelListItems( mTransformFolders.value, items )
            }

            return result
        }

    val results: LiveData<List<MainViewModelListItem>>
        = Transformations.switchMap(showSearchResults) {
            if (it) searchResults else listItems
        }

    init {
        mCurFolderId.value = null
        searchQuery.value = null

        isFilesLoading.value = true
        isFoldersLoading.value = true
    }

    // Methods

    fun setCurFolder(id: Long?) {
        mCurFolderId.value = id
    }

    fun setCurFolderUp() {
        mCurFolderId.value = mCurFolder.value?.ParentId
    }

    fun getCurFolder(): EncryptedFolder? {
        return mCurFolder.value
    }

    fun setSearchQuery(query: String?) {
        searchQuery.value = query
    }

    fun getSearchQuery(): String? {
        return searchQuery.value
    }

    private fun encryptedFolderToListItem(i: EncryptedFolder): MainViewModelListItem {
        return object : MainViewModelListItem {

            override val id: Long = i.Id

            override val item: Any
                get() = i

            override val menuResource: Int = R.menu.folder_menu

            override val name: String? =
                if (decryptor.profile.Id != i.ProfileId)
                    null
                else try {
                    String(decryptor.decrypt(i.Name))
                } catch (e: EncryptException) {
                    null
            }

            override val drawableRes: Int = R.drawable.ic_outline_folder_24px

            override val stream: InputStream? = null

            override val stableId: Long = (i.Id xor Long.MAX_VALUE)

            override fun showLockIcon(): Boolean {
                return false
            }

            override fun showMenuOptions(): Boolean {
                return true
            }

            override fun showPreview(): Boolean {
                return false
            }

            override fun isDirectory(): Boolean {
                return true
            }

            override fun rename(name: String) {
                try {
                    renameEncryptedFolder(i, name.toByteArray())
                } catch (ignored: EncryptException) {
            } }

            override fun move(parent: EncryptedFolder?) {
                moveEncryptedFolder(i, parent)
    } } }

    private fun encryptedFileToListItem(i: EncryptedFile): MainViewModelListItem {
        return object : MainViewModelListItem {

            override val id: Long = i.Id

            override val item: Any
                get() = i

            override val menuResource: Int = R.menu.item_menu;

            override val name: String? =
                    if (decryptor.profile.Id != i.ProfileId)
                        null
                    else try {
                        String(decryptor.decrypt(i.Name))
                    } catch (e: EncryptException) {
                        null
                    }

            override val drawableRes: Int = Utils.getDrawableResFromType(
                    Utils.getFileTypeFromName(this.name))

            override val stream: InputStream?
                    get() { return mRepository.readEncryptedFile(i.FilesystemId) }

            override val stableId: Long = i.Id

            override fun showLockIcon(): Boolean {
                return decryptor.profile.Id != i.ProfileId
            }

            override fun showMenuOptions(): Boolean {
                return true
            }

            override fun showPreview(): Boolean {
                return try {
                    Utils.getFileTypeFromName(
                            String(decryptor.decrypt(i.Name))) == FileType.IMAGE
                } catch (e: EncryptException) {
                    false
            } }

            override fun isDirectory(): Boolean {
                return false
            }

            override fun rename(name: String) {
                try {
                    renameEncryptedFile(i, name.toByteArray())
                } catch (ignored: EncryptException) {
            } }

            override fun move(parent: EncryptedFolder?) {
                moveEncryptedFile(i, parent)
    } } }

    private fun getFilesFromListItems(items: List<MainViewModelListItem>): List<EncryptedFile> {
        val result = ArrayList<EncryptedFile>()

        items.forEach {
            if (!it.isDirectory())
                result.add(it.item as EncryptedFile)
        }
        return result
    }

    private fun getFoldersFromListItems(items: List<MainViewModelListItem>): List<EncryptedFolder> {
        val result = ArrayList<EncryptedFolder>()

        items.forEach {
            if (it.isDirectory())
                result.add(it.item as EncryptedFolder)
        }
        return result
    }

    private fun combineViewModelListItems(l0: List<MainViewModelListItem>?
            , l1: List<MainViewModelListItem>?): List<MainViewModelListItem> {
        val l2 = ArrayList<MainViewModelListItem>()
        if (l0 != null)
            l2.addAll(l0)
        if (l1 != null)
            l2.addAll(l1)
        return l2
    }

    // Action methods

    fun importFiles(uris: List<Uri>, deleteOnComplete: Boolean = false
                    , contentResolver: ContentResolver)
            = mRepository.addEncryptedFiles(uris, getCurFolder(), deleteOnComplete, contentResolver)

    fun newFolder(name: ByteArray, parent: EncryptedFolder?)
            = mRepository.addEncryptedFolder(name, parent)

    fun exportItems(items: List<MainViewModelListItem>, path: String, deleteOnComplete: Boolean) {
        exportEncryptedFolders(getFoldersFromListItems(items), path, deleteOnComplete)
        exportEncryptedFiles(getFilesFromListItems(items), path, deleteOnComplete)
    }

    fun deleteItems(items: List<MainViewModelListItem>) {
        deleteEncryptedFolders(getFoldersFromListItems(items))
        deleteEncryptedFiles(getFilesFromListItems(items))
    }

    private fun exportEncryptedFiles(files: List<EncryptedFile>, path: String
                     , deleteOnComplete: Boolean)
            = mRepository.exportEncryptedFiles(files, path, deleteOnComplete)

    private fun exportEncryptedFolders(folders: List<EncryptedFolder>, path: String
                     , deleteOnComplete: Boolean)
            = mRepository.exportEncryptedFolders(folders, path, deleteOnComplete)

    private fun deleteEncryptedFiles(files: List<EncryptedFile>)
            = mRepository.deleteEncryptedFiles(files)

    private fun deleteEncryptedFolders(folders: List<EncryptedFolder>)
            = mRepository.deleteEncryptedFolders(folders)

    private fun renameEncryptedFile(file: EncryptedFile, name: ByteArray)
            = mRepository.renameEncryptedFile(file, name)

    private fun renameEncryptedFolder(folder: EncryptedFolder, name: ByteArray)
            = mRepository.renameEncryptedFolder(folder, name)

    private fun moveEncryptedFile(file: EncryptedFile, parent: EncryptedFolder?)
            = mRepository.moveEncryptedFile(file, parent)

    private fun moveEncryptedFolder(folder: EncryptedFolder, parent: EncryptedFolder?)
            = mRepository.moveEncryptedFolder(folder, parent)
}
