package com.encrypt.midford.ki;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.encrypt.midford.ki.encrypt.Key;
import com.encrypt.midford.ki.room.Profile;

public class SetupFragment extends Fragment implements TextWatcher, OnClickListener {

    private static final String PASS_1_TAG = "PASS_1";
    private static final String PASS_2_TAG = "PASS_2";

    private AppCompatActivity mActivity;
    private SetupViewModel mViewModel;

    private LinearLayout strength;
    private ProgressBar progress;
    private EditText pass1;
    private EditText pass2;
    private TextView create;
    private TextView options;

    private ImageView level1;
    private ImageView level2;
    private ImageView level3;

    // Constructor

    public SetupFragment() { /* Required empty public constructor */ }

    // Setup

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity)
            mActivity = (AppCompatActivity) context;
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setup_frag, container, false);

        strength = view.findViewById(R.id.ll_strength);
        create = view.findViewById(R.id.tv_create);
//        options = view.findViewById(R.id.tv_adv_options);
        pass1 = view.findViewById(R.id.password_setup_et);
        pass2 = view.findViewById(R.id.password_et_verify);
        progress = view.findViewById(R.id.setup_progress);

        create.setOnClickListener(this);
//        options.setOnClickListener(this);

        level1 = view.findViewById(R.id.level_1);
        level2 = view.findViewById(R.id.level_2);
        level3 = view.findViewById(R.id.level_3);

        pass1.addTextChangedListener(this);
        pass2.addTextChangedListener(this);

        if (savedInstanceState != null) {
            pass1.setText(savedInstanceState.getString(PASS_1_TAG));
            pass2.setText(savedInstanceState.getString(PASS_2_TAG));
        }

        mViewModel = ViewModelProviders.of(this).get(SetupViewModel.class);

        mViewModel.getStatus().observe(getViewLifecycleOwner()
                , new Observer<SetupViewModel.SetupBundle>() {
                    @Override
                    public void onChanged(SetupViewModel.SetupBundle bundle) {
                        SetupViewModel.SetupBundle.Status code = bundle.getStatus();

                        switch(code) {
                            case LOADING:
                                progress.setVisibility(View.VISIBLE);
                                pass1.setEnabled(false);
                                pass2.setEnabled(false);
                                create.setEnabled(false);
                                break;
                            case SUCCEEDED:
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                intent.putExtra(Key.class.getName(), bundle.getKey());
                                intent.putExtra(Profile.class.getName(), bundle.getProfile());
                                startActivity(intent);
                                mActivity.finish();

                                if (mActivity != null)
                                    mActivity.finish();
                                break;
                            case FAILED:
                                if (bundle.getException() != null) {
                                    pass1.setError(bundle.getException().getMessage());
                                    Log.e(SetupFragment.class.getName()
                                            , bundle.getException().getMessage());
                                }
                            default:
                                progress.setVisibility(View.INVISIBLE);
                                pass1.setEnabled(true);
                                pass2.setEnabled(true);
                                create.setEnabled(true);
                        }
        } } );

        mViewModel.getStrength().observe(getViewLifecycleOwner()
                , new Observer<SetupViewModel.Strength>() {
                    @Override
                    public void onChanged(SetupViewModel.Strength strength) {
                        switch (strength) {
                            case WEAK:
                                setWeakBar();
                                break;
                            case MEDIUM:
                                setMediumBar();
                                break;
                            case STRONG:
                                setStrongBar();
                                break;
                        }
        } } );

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(PASS_1_TAG, pass1.getText().toString());
        outState.putString(PASS_2_TAG, pass2.getText().toString());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    // Methods

    @Override
    public void onClick(View v) {
//        if (v.getId() == R.id.tv_adv_options)
//            showAdvancedOptionsDialogue();
//        else
        if (mViewModel != null)
            mViewModel.setup(pass1.getText().toString(), pass2.getText().toString());
    }

    private void setWeakBar() {
        level1.setVisibility(View.VISIBLE);
        level2.setVisibility(View.INVISIBLE);
        level3.setVisibility(View.INVISIBLE);
        level1.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.StrengthLevel1));
        strength.setContentDescription(getResources().getString(R.string.strength_low_description));
    }

    private void setMediumBar() {
        level1.setVisibility(View.VISIBLE);
        level2.setVisibility(View.VISIBLE);
        level3.setVisibility(View.INVISIBLE);
        level1.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.StrengthLevel2));
        level2.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.StrengthLevel2));
        strength.setContentDescription(getResources().getString(R.string.strength_medium_description));
    }

    private void setStrongBar() {
        level1.setVisibility(View.VISIBLE);
        level2.setVisibility(View.VISIBLE);
        level3.setVisibility(View.VISIBLE);
        level1.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.StrengthLevel3));
        level2.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.StrengthLevel3));
        level3.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.StrengthLevel3));
        strength.setContentDescription(getResources().getString(R.string.strength_high_description));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (mViewModel != null)
            mViewModel.setPhrase(pass1.getText().toString());
    }

    @Override
    public void afterTextChanged(Editable s) {
        create.setEnabled(!pass1.getText().toString().isEmpty()
                && !pass2.getText().toString().isEmpty());
    }

    private void showAdvancedOptionsDialogue() {
        new AdvancedOptionsDialogue(mActivity)
            .show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnSetupFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
} }
