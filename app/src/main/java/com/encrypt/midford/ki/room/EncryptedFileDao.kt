package com.encrypt.midford.ki.room

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface EncryptedFileDao {

    @Query("SELECT * FROM file_items")
    fun getAllFiles(): LiveData<List<EncryptedFile>?>

    @Query("SELECT * FROM file_items WHERE id=:id")
    fun getFile(id: Long): LiveData<EncryptedFile?>

    @Query("SELECT * FROM file_items WHERE parent IS NULL")
    fun getRootFiles(): LiveData<List<EncryptedFile>?>

    @Query("SELECT * FROM file_items WHERE parent=:parentId")
    fun getFiles(parentId: Long?): LiveData<List<EncryptedFile>?>

    @Query("SELECT * FROM file_items WHERE parent=:parentId")
    fun getFilesSync(parentId: Long?): List<EncryptedFile>?

    @Insert
    fun insertFile(encryptedFile: EncryptedFile)

    @Update
    fun updateFile(encryptedFile: EncryptedFile)

    @Delete
    fun deleteFile(encryptedFile: EncryptedFile)
}
