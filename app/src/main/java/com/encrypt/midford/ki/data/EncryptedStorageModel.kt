package com.encrypt.midford.ki.data

import android.content.ContentResolver
import android.net.Uri
import com.encrypt.midford.ki.encrypt.Encryptor
import com.encrypt.midford.ki.room.EncryptedFile
import com.encrypt.midford.ki.room.EncryptedFolder
import com.encrypt.midford.ki.utils.Paths
import java.io.File
import java.io.InputStream
import java.util.*

internal class EncryptedStorageModel(
        private val mEncryptor: Encryptor
) {

    private val FILEPATH = File(Paths.STORAGE_PATH)

    private fun generateRandomId(): String {
        return UUID.randomUUID().toString()
    }

    private val SANITIZE_SUBSTRING = "_"
    private fun sanitizeFileName(name: String): String {
        return name.replace("\\", SANITIZE_SUBSTRING)
            .replace("/", SANITIZE_SUBSTRING)
            .replace(":", SANITIZE_SUBSTRING)
            .replace("*", SANITIZE_SUBSTRING)
            .replace("?", SANITIZE_SUBSTRING)
            .replace("\"", SANITIZE_SUBSTRING)
            .replace("<", SANITIZE_SUBSTRING)
            .replace(">", SANITIZE_SUBSTRING)
            .replace("|", SANITIZE_SUBSTRING)
    }

    private fun generateVersionSubstring(id: Int?): String {
        return if (id == null)
                ""
            else
                String.format("_%02d", id)
    }

    private fun extractFileExtension(name: String): Pair<String, String> {
        val start = name.substringBeforeLast(".")
        val end = name.substringAfterLast(".")

        return Pair(start, ".$end")
    }

    private fun generateExportName(path: File, name: String): String {
        var candidate = sanitizeFileName(name)
        var file = File(path, candidate)

        var i = 1
        while(file.exists()) {
            val sanitize = sanitizeFileName(name)
            val result = extractFileExtension(sanitize)
            val ver = generateVersionSubstring(i)
            candidate = "${result.first}$ver${result.second}"

            file = File(path, candidate)
            i++
        }

        return candidate
    }

    private fun getEncryptedFileStream(id: String): InputStream? {
        val file = File(FILEPATH, id)

        return if (file.exists()) file.inputStream() else null
    }

    fun readFile(id: String): InputStream? {
        val stream = getEncryptedFileStream(id)
        return if (stream != null) mEncryptor.getDecryptStream(stream) else null
    }

    fun writeFile(stream: InputStream): Pair<Boolean, String> {
        val id = generateRandomId()
        val file = File(FILEPATH, id)

        var result = FILEPATH.exists() || FILEPATH.mkdirs()
        result = result && (!file.exists() && file.createNewFile())

        file.outputStream().use {
            try {
                mEncryptor.encryptStream(stream, it)
            } catch (e: Exception) {
                result = false
            }
        }

        return Pair(result, id)
    }

    fun exportFile(file: EncryptedFile, path: File): Boolean {
        var result = path.exists() || path.mkdirs()

        val title = String(mEncryptor.decrypt(file.Name))
        val name = generateExportName(path, title)
        val exportPath = File(path, name)

        result = result && exportPath.createNewFile()

        val inputStream = getEncryptedFileStream(file.FilesystemId);

        inputStream?.use { input ->
            exportPath.outputStream().use { output ->
                try {
                    mEncryptor.decryptStream(input, output)
                } catch (e: Exception) {
                    result = false
                }
            }
        }

        return result
    }

    fun exportFolder(folder: EncryptedFolder, path: File): Pair<Boolean, String> {
        val title = String(mEncryptor.decrypt(folder.Name))
        val name = sanitizeFileName(title)
        val exportPath = File(path, name)

        val result = exportPath.exists() || exportPath.mkdirs()

        return Pair(result, name)
    }

    fun deleteFile(id: String): Boolean {
        val file = File(FILEPATH, id)
        return file.exists() && file.delete()
    }

    fun deleteFileFromUri(uri: Uri, contentResolver: ContentResolver): Boolean {

        return contentResolver.delete(uri, null, null) > 0
    }

    companion object {

        @Volatile private var instance: EncryptedStorageModel? = null

        fun getInstance(encryptor: Encryptor) =
                instance ?: synchronized(this) {
                    instance
                            ?: EncryptedStorageModel(encryptor).also {
                                instance = it
                            }
                }
    }
}
