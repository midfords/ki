package com.encrypt.midford.ki;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.signature.ObjectKey;

import com.encrypt.midford.ki.components.CustomDialogueBuilder;
import com.encrypt.midford.ki.encrypt.Encryptor;
import com.encrypt.midford.ki.glide.GlideApp;
import com.encrypt.midford.ki.utils.FileType;
import com.encrypt.midford.ki.utils.Paths;

public class PreviewActivity extends AppCompatActivity implements View.OnClickListener
        , PopupMenu.OnMenuItemClickListener {

    private PreviewViewModel mViewModel;
    private Encryptor mEncryptor;

    private LinearLayout mActionBar;
    private TextView mTitle;
    private TextView mErrorView;
    private ImageView mPreviewImageView;
    private PreviewViewModel.PreviewItem mItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        Intent intent = getIntent();
        mEncryptor = getIntent().getParcelableExtra(Encryptor.class.getName());
        long id = intent.getLongExtra("Id", -1);

        if (id == -1)
            finish();

        final ImageView menu = findViewById(R.id.iv_preview_menu);
        final ImageView back = findViewById(R.id.iv_preview_back);
        menu.setOnClickListener(this);
        back.setOnClickListener(this);

        mPreviewImageView = findViewById(R.id.summary_image_view);
        mActionBar = findViewById(R.id.preview_action_bar);
        mTitle = findViewById(R.id.tv_preview_title);
        mErrorView = findViewById(R.id.preview_error_view);

        mPreviewImageView.setOnClickListener(this);

        mViewModel = ViewModelProviders.of(this
                , new PreviewViewModelFactory(getApplication(), mEncryptor))
                .get(PreviewViewModel.class);

        mViewModel.getPreviewItem().observe(this
                , new Observer<PreviewViewModel.PreviewItem>() {
            @Override
            public void onChanged(PreviewViewModel.PreviewItem item) {
                try {
                    mItem = item;
                    mTitle.setText(item.getName());

                    if (item.getType() != FileType.IMAGE) {
                        showErrorMessage();
                    } else {

                        GlideApp.with(mPreviewImageView.getContext())
                                .load(item.getStream())
                                .fitCenter()
                                .transition(DrawableTransitionOptions.withCrossFade(5))
                                .signature(new ObjectKey(item.getId()))
                                .into(mPreviewImageView);
                    }
                } catch (Exception ignored) {
                    showErrorMessage();
                    mTitle.setText(null);
                }
        } } );

        mViewModel.setFileId(id);
    }

    private void showErrorMessage() {
        mErrorView.setVisibility(View.VISIBLE);
        mPreviewImageView.setVisibility(View.GONE);
    }

    private void toggleActionBar() {
        if (mActionBar.getVisibility() == View.VISIBLE)
            mActionBar.setVisibility(View.GONE);
        else
            mActionBar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (mItem == null)
            return false;

        switch (item.getItemId()) {
            case R.id.action_export:
                showExportDialogue(mItem);
                return true;
            case R.id.action_delete:
                showDeleteDialogue(mItem);
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.summary_image_view:
                toggleActionBar();
                break;
            case R.id.iv_preview_menu:
                PopupMenu popupSelect = new PopupMenu(this, v);
                popupSelect.getMenuInflater().inflate(R.menu.preview, popupSelect.getMenu());
                popupSelect.setOnMenuItemClickListener(this);
                popupSelect.show();
                break;
            case R.id.iv_preview_back:
                finish();
                break;
        }
    }

    private void showExportDialogue(@NonNull final PreviewViewModel.PreviewItem item) {
        final CheckBox cb = new CheckBox(this);
        cb.setText(getResources().getQuantityString(
                R.plurals.delete_from_ki_msg, 1, 1));

        final String path = Paths.EXPORT_PATH;
        final LinearLayout pathLayout = (LinearLayout) getLayoutInflater()
                .inflate(R.layout.comp_dialogue_path, null);
        TextView message = pathLayout.findViewById(R.id.tv_destination_msg);
        message.setText(path);

        CustomDialogueBuilder
                .fromContext(this)
                .setIcon(R.drawable.ic_baseline_export_24px)
                .setTitle(R.string.export_title)
                .setMessage(getResources().getQuantityString(
                        R.plurals.export_msg, 1, 1))
                .addContentView(pathLayout)
                .addContentView(cb)
                .setPrimaryActionText(R.string.export_btn)
                .setSecondaryActionText(R.string.cancel)
                .setPrimaryActionListener(new CustomDialogueBuilder.Action() {
                    @Override
                    public void onAction(@NonNull Context context) {
                        item.export(path, cb.isChecked());

                        if (cb.isChecked())
                            finish();
                    } } )
                .show();
    }

    private void showDeleteDialogue(@NonNull final PreviewViewModel.PreviewItem item) {

        CustomDialogueBuilder
                .fromContext(this)
                .setIcon(R.drawable.ic_outline_delete_24px)
                .setTitle(R.string.delete_title)
                .setMessage(getResources().getQuantityString(
                        R.plurals.delete_msg, 1, 1))
                .setPrimaryActionText(R.string.action_delete)
                .setSecondaryActionText(R.string.cancel)
                .setPrimaryActionListener(new CustomDialogueBuilder.Action() {
                    @Override
                    public void onAction(@NonNull Context context) {
                        item.delete();
                        finish();
                    }
                })
                .show();
    }
}
