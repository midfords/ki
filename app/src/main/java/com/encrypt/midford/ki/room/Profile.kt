package com.encrypt.midford.ki.room

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
          tableName = "profiles"
)

data class Profile(
        @PrimaryKey(autoGenerate = true) @NonNull
        @ColumnInfo(name = "id")
        var Id: Long,

        @ColumnInfo(name = "hash", typeAffinity = ColumnInfo.BLOB)
        var Hash: ByteArray,

        @ColumnInfo(name = "salt", typeAffinity = ColumnInfo.BLOB)
        var Salt: ByteArray,

        @ColumnInfo(name = "cipher")
        var Cipher: String,

        @ColumnInfo(name = "digest")
        var Digest: String,

        @ColumnInfo(name = "keystore")
        var KeyStore: String
) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.createByteArray()!!,
            parcel.createByteArray()!!,
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readString()!!)

    override fun equals(other: Any?): Boolean {
        return this === other
                || ( javaClass == other?.javaClass
                && Id == (other as EncryptedFolder).Id )
    }

    override fun hashCode(): Int {
        return Id.hashCode()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(Id)
        parcel.writeByteArray(Hash)
        parcel.writeByteArray(Salt)
        parcel.writeString(Cipher)
        parcel.writeString(Digest)
        parcel.writeString(KeyStore)
    }

    override fun describeContents(): Int {
        return hashCode()
    }

    companion object CREATOR : Parcelable.Creator<Profile> {
        override fun createFromParcel(parcel: Parcel): Profile {
            return Profile(parcel)
        }

        override fun newArray(size: Int): Array<Profile?> {
            return arrayOfNulls(size)
        }
    }
}
