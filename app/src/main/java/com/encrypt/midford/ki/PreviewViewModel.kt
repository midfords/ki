package com.encrypt.midford.ki

import android.app.Application
import androidx.lifecycle.*

import com.encrypt.midford.ki.encrypt.Encryptor

import com.encrypt.midford.ki.data.AppRepository
import com.encrypt.midford.ki.encrypt.EncryptException
import com.encrypt.midford.ki.room.EncryptedFile
import com.encrypt.midford.ki.utils.FileType
import com.encrypt.midford.ki.utils.Utils

import java.io.InputStream

internal class PreviewViewModel(
          app: Application
        , mDecryptor: Encryptor
) : AndroidViewModel(app) {

    private val mRepository: AppRepository = AppRepository(app, mDecryptor)

    private val mFileId: MutableLiveData<Long> = MutableLiveData()

    private val mFile: LiveData<EncryptedFile?>
        = Transformations.switchMap(mFileId) { id ->

        mRepository.getEncryptedFile(id)
    }

    val previewItem: LiveData<PreviewItem>
        = Transformations.map(mFile) {

        if (it == null) {
            object : PreviewItem {
                override fun getStream(): InputStream? { return null }
                override fun getId(): Long { return -1 }
                override fun getName(): String? { return null }
                override fun getType(): FileType { return FileType.GENERIC }
                override fun delete() {}
                override fun export(root: String, deleteOnComplete: Boolean) {}
            }

        } else {

            object : PreviewItem {
                override fun getStream(): InputStream? {
                    return mRepository.readEncryptedFile(it.FilesystemId)
                }

                override fun getId(): Long {
                    return it.Id
                }

                override fun getName(): String? {
                    return if (mDecryptor.profile.Id != it.ProfileId)
                        null
                    else try {
                        String(mDecryptor.decrypt(it.Name))
                    } catch (e: EncryptException) {
                        null
                    }
                }

                override fun getType(): FileType {
                    return try {
                        Utils.getFileTypeFromName(String(mDecryptor.decrypt(it.Name)))
                    } catch (e: EncryptException) {
                        FileType.GENERIC
                    }
                }

                override fun delete() {
                    deleteEncryptedFile(it)
                }

                override fun export(root: String, deleteOnComplete: Boolean) {
                    exportEncryptedFile(it, root, deleteOnComplete)
                }
            }
        }
    }

    fun setFileId(id: Long) {
        mFileId.value = id
    }

    private fun exportEncryptedFile(file: EncryptedFile, path: String, deleteOnComplete: Boolean)
            = mRepository.exportEncryptedFile(file, path, deleteOnComplete)

    private fun deleteEncryptedFile(file: EncryptedFile)
            = mRepository.deleteEncryptedFile(file)

    interface PreviewItem {
        fun getStream(): InputStream?

        fun getId(): Long

        fun getName(): String?

        fun getType(): FileType

        fun delete()

        fun export(root: String, deleteOnComplete: Boolean)
    }
}
