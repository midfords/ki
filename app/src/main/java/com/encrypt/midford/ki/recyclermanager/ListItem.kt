package com.encrypt.midford.ki.recyclermanager

import java.io.InputStream

interface ListItem {

    val stableId: Long

    val name: String?

    val menuResource: Int

    val drawableRes: Int

    val stream: InputStream?

    fun showLockIcon(): Boolean

    fun showMenuOptions(): Boolean

    fun showPreview(): Boolean
}
