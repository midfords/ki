package com.encrypt.midford.ki.glide;

import android.util.Log;

import androidx.annotation.NonNull;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;

import java.io.IOException;
import java.io.InputStream;

public class InputStreamDataFetcher implements DataFetcher<InputStream> {

    private final InputStream model;

    InputStreamDataFetcher(InputStream model) {
        this.model = model;
    }

    @Override
    public void loadData(@NonNull Priority priority
            , @NonNull DataCallback<? super InputStream> callback) {
        callback.onDataReady(model);
    }

    @Override
    public void cleanup() {
        try {
            model.close();
        } catch (IOException e) {
            Log.w("PassthroughDataFetcher", "Cannot clean up after stream.", e);
        }
    }

    @Override
    public void cancel() {
        try {
            model.close();
        } catch (Exception e) {
            Log.w("PassthroughDataFetcher", "Cannot clean up after stream.", e);
        }
    }

    @NonNull
    @Override
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @NonNull
    @Override
    public DataSource getDataSource() {
        return DataSource.LOCAL;
    }
}
