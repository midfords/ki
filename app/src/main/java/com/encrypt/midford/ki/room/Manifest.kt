package com.encrypt.midford.ki.room

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
        tableName = "manifest"
      , foreignKeys = [
          ForeignKey(
              entity = Profile::class
            , parentColumns = arrayOf("id")
            , childColumns = arrayOf("profile")
) ] )

data class Manifest(
        @PrimaryKey @NonNull
        @ColumnInfo(name = "profile")
        var DefaultProfileId: Long
) {

    override fun equals(other: Any?): Boolean {
        return this === other
                || ( javaClass == other?.javaClass
                && DefaultProfileId == (other as EncryptedFolder).Id )
    }

    override fun hashCode(): Int {
        return DefaultProfileId.hashCode()
    }
}
