package com.encrypt.midford.ki.recyclermanager

import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.signature.ObjectKey
import com.encrypt.midford.ki.R
import com.encrypt.midford.ki.glide.GlideApp
import java.io.InputStream

class ListItemAdapter<T : ListItem>(private val listener: ViewHolderOnClickListener<T>)
    : ListAdapter<T, ListItemAdapter.ListItemViewHolder<T>>(ListItemDiffCallback<T>()) {

    private var tracker: SelectionTracker<Long>? = null

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemViewHolder<T> {
        val inflater = LayoutInflater.from(parent.context)
        return ListItemViewHolder(
                inflater.inflate(R.layout.comp_list_item, parent, false), tracker)
    }

    override fun onBindViewHolder(holder: ListItemViewHolder<T>, position: Int) {
        val item = getItem(position)
        holder.bind(item, tracker?.isSelected(item.stableId) ?: false, listener)
    }

    class ListItemViewHolder<U : ListItem>(private val view: View
                                           , private val tracker: SelectionTracker<Long>?)
        : RecyclerView.ViewHolder(view) {

        private val name = view.findViewById<TextView?>(R.id.item_title)
        private val icon = view.findViewById<ImageView?>(R.id.icon_image)
        private val lock = view.findViewById<ImageView?>(R.id.lock_image)
        private val menu = view.findViewById<ImageView?>(R.id.list_item_sub_menu)

        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> {
            return object: ItemDetailsLookup.ItemDetails<Long>() {

                override fun getPosition(): Int {
                    return adapterPosition
                }

                override fun getSelectionKey(): Long {
                    return itemId
                }
        } }

        private fun setImage(stream: InputStream?, def: Int, id: Long) {
            if (icon == null) return

            try {
                GlideApp.with(view.context)
                        .load(stream)
                        .fallback(def)
                        .fitCenter()
                        .circleCrop()
                        .transition(DrawableTransitionOptions.withCrossFade(25))
                        .signature(ObjectKey(id))
                        .into(icon)
            } catch (ignored: Exception) { }
        }

        fun bind(item: U, isSelected: Boolean, listener: ViewHolderOnClickListener<U>) {
            name?.text = item.name
            lock?.visibility = if(item.showLockIcon()) VISIBLE else GONE
            menu?.visibility = if(item.showMenuOptions()) VISIBLE else GONE
            setImage( if(item.showPreview() && item.stream != null) item.stream else null
                    , item.drawableRes
                    , item.stableId)
            view.isActivated = isSelected

            menu?.setOnClickListener { v ->
                if(tracker?.hasSelection() != true) listener.onClick(item, v) }
            view.setOnClickListener { v ->
                if(tracker?.hasSelection() != true) listener.onClick(item, v) }
        }
    }

    fun setTracker(value: SelectionTracker<Long>?) {
        tracker = value
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).stableId
    }

    interface ViewHolderOnClickListener<T> {
        fun onClick(i: T, v: View)
    }
}