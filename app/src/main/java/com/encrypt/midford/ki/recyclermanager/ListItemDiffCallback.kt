package com.encrypt.midford.ki.recyclermanager

import androidx.recyclerview.widget.DiffUtil

class ListItemDiffCallback<T : ListItem> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem.stableId == newItem.stableId
    }

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem.name == newItem.name
    }
}