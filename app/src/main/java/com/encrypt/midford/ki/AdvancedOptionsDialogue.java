package com.encrypt.midford.ki;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class AdvancedOptionsDialogue extends Dialog implements View.OnClickListener{

    private AppCompatActivity mActivity;
    private OnFinishClickListener mListener;

    // Constructor

    AdvancedOptionsDialogue(@NonNull AppCompatActivity activity) {
        super(activity);
        mActivity = activity;
    }

    // Handlers

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.algorithms_dialogue);

        Spinner cipher = findViewById(R.id.spinner_cipher);
        Spinner digest = findViewById(R.id.spinner_digest);
        Spinner keyStore = findViewById(R.id.spinner_ks);
        TextView cancelBtn = findViewById(R.id.tv_cancel);
        TextView applyBtn = findViewById(R.id.tv_apply);

        cancelBtn.setOnClickListener(this);
        applyBtn.setOnClickListener(this);


    }

    @Override
    public void dismiss() {
        mActivity = null;
        if (mListener != null)
            mListener.onFinish();
        super.dismiss();
    }

    @Override
    public void cancel() {
        mActivity = null;
        if (mListener != null)
            mListener.onFinish();
        super.cancel();
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_apply:
                dismiss();
                break;
            case R.id.tv_cancel:
                cancel();
                break;
    } }

    // Interfaces

    interface OnFinishClickListener {
        void onFinish();
    }
}
