package com.encrypt.midford.ki.utils;

import java.util.Date;

import androidx.room.TypeConverter;

public class Converter {

    @TypeConverter
    public static FileType toType(int value) {
        return FileType.fromInt(value);
    }

    @TypeConverter
    public static int toInt(FileType value) {
        return value.toInt();
    }

    @TypeConverter
    public static Date toDate(long value) {
        return new Date(value);
    }

    @TypeConverter
    public static long toString(Date value) {
        return value.getTime();
    }
}
