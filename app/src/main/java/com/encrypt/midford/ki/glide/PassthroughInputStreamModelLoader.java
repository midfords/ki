package com.encrypt.midford.ki.glide;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import com.bumptech.glide.signature.ObjectKey;

import java.io.InputStream;

public final class PassthroughInputStreamModelLoader implements
        ModelLoader<InputStream, InputStream> {

    @Override
    public LoadData<InputStream> buildLoadData(@NonNull InputStream model, int width
            , int height, @NonNull Options options) {
        return new LoadData<>(new ObjectKey(model), new InputStreamDataFetcher(model));
    }

    @Override
    public boolean handles(@NonNull InputStream model) {
        return true;
    }

    public static class Factory implements ModelLoaderFactory<InputStream, InputStream> {

        @NonNull
        @Override
        public ModelLoader<InputStream, InputStream> build(
                @NonNull MultiModelLoaderFactory multiFactory) {
            return new PassthroughInputStreamModelLoader();
        }

        @Override public void teardown() { }
    }
}
