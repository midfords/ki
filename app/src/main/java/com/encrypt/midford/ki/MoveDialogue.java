package com.encrypt.midford.ki;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.encrypt.midford.ki.MoveViewModel.MoveListItem;

import com.encrypt.midford.ki.components.CustomDialogueBuilder;
import com.encrypt.midford.ki.encrypt.Encryptor;
import com.encrypt.midford.ki.recyclermanager.RecyclerViewManager;
import com.encrypt.midford.ki.recyclermanager.RecyclerViewManagerBuilder;
import com.encrypt.midford.ki.room.EncryptedFolder;
import com.encrypt.midford.ki.utils.Utils;

import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class MoveDialogue extends Dialog implements View.OnClickListener
        , RecyclerViewManager.OnListItemClickListener<MoveListItem>{

    private AppCompatActivity mActivity;
    private EncryptedFolder mCur;
    private String mDefault;

    private Encryptor mEncryptor;
    private MoveViewModel mViewModel;
    private RecyclerViewManager<MoveListItem> mManager;

    private List<MainViewModelListItem> mItems;
    private ActionListener mListener;

    private TextView mTitle;
    private ImageView mBack;

    // Constructor

    MoveDialogue(@NonNull AppCompatActivity activity) {
        super(activity);
        mActivity = activity;
    }

    // Handlers

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.move_dialogue);

        mTitle = findViewById(R.id.tv_title);
        mBack = findViewById(R.id.iv_back_arrow);
        ImageView mAddFolder = findViewById(R.id.iv_new_folder);
        TextView cancelBtn = findViewById(R.id.tv_cancel);
        TextView moveBtn = findViewById(R.id.tv_move);

        mBack.setOnClickListener(this);
        mAddFolder.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        moveBtn.setOnClickListener(this);

        if (mManager == null) {

            RecyclerViewManagerBuilder<MoveListItem> builder
                    = new RecyclerViewManagerBuilder<>(getContext());

            mManager = builder
                    .setEmptyStateText(R.string.empty_move_message)
                    .setComparator(new Comparator<MoveListItem>() {
                        @Override
                        public int compare(MoveListItem obj1, MoveListItem obj2) {
                            return obj1.getName().compareToIgnoreCase(obj2.getName());
                        }
                    })
                    .setOnListItemClickListener(this)
                    .getManager();
        }

        RelativeLayout containerLayout = findViewById(R.id.fragment_container);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, Utils.convertDpToPx(mActivity, 200f));

        View mainView = mManager.getView();
        containerLayout.addView(mainView, params);

        mViewModel = ViewModelProviders.of(mActivity
                , new MoveViewModelFactory(mActivity.getApplication(), mEncryptor))
                .get(MoveViewModel.class);

        mViewModel.getFolderListItems().observe(mActivity, new Observer<List<MoveListItem>>() {
            @Override
            public void onChanged(List<MoveListItem> items) {
                mManager.setItems(items);
        } } );

        mViewModel.isRootFolder().observe(mActivity, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean b) {
                mBack.setVisibility(b ? View.INVISIBLE : View.VISIBLE);
            }
        });

        mViewModel.getCurFolderName().observe(mActivity, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                mTitle.setText( s == null ? mDefault : s);
            }
        });

        mViewModel.setItems(mItems);
        mViewModel.setCurFolder(mCur == null ? null : mCur.getId());
    }

    private void success() {
        mActivity = null;
        if (mListener != null)
            mListener.onSuccess();
        dismiss();
    }

    @Override
    public void cancel() {
        mActivity = null;
        if (mListener != null)
            mListener.onCancel();
        super.cancel();
    }

    @Override
    public void onListItemClicked(View v, MoveListItem i) {
        mViewModel.setCurFolder(i.getId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_move:
                for (MainViewModelListItem i : mViewModel.getItems())
                    i.move(mViewModel.getCurFolder());
                success();
                break;
            case R.id.tv_cancel:
                cancel();
                break;
            case R.id.iv_back_arrow:
                mViewModel.setCurFolderUp();
                break;
            case R.id.iv_new_folder:
                showNewFolderDialogue();
                break;
    } }

    private void showNewFolderDialogue() {
        final EditText et = new EditText(mActivity);
        et.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        et.setHint(R.string.new_folder_hint);
        et.setText(R.string.new_folder_hint);
        et.setSelectAllOnFocus(true);

        CustomDialogueBuilder
                .fromContext(mActivity)
                .setIcon(R.drawable.ic_outline_create_new_folder_24px)
                .setTitle(R.string.new_folder_title)
                .addContentView(et)
                .setPrimaryActionText(R.string.ok)
                .setSecondaryActionText(R.string.cancel)
                .setPrimaryActionListener(new CustomDialogueBuilder.Action() {
                    @Override
                    public void onAction(Context context) {
                        if (et.getText().toString().isEmpty()) {
                            et.setError(mActivity.getResources()
                                    .getString(R.string.no_text_err));
                            return;
                        }

                        mViewModel.addEncryptedFolder(
                                et.getText().toString().getBytes(), mViewModel.getCurFolder());
                    }
                })
                .setDismissCondition(new CustomDialogueBuilder.DismissCondition() {
                    @Override
                    public boolean shouldDismiss() {
                        return !et.getText().toString().isEmpty();
                    }
                })
                .show();
    }

    // Setters

    MoveDialogue setCurFolder(EncryptedFolder folder) {
        mCur = folder;
        return this;
    }

    MoveDialogue setDefaultFolderName(String s) {
        mDefault = s;
        return this;
    }

    MoveDialogue setEncryptor(Encryptor encryptor) {
        mEncryptor = encryptor;
        return this;
    }

    MoveDialogue setItems(List<MainViewModelListItem> items) {
        mItems = items;
        return this;
    }

    MoveDialogue setActionListener(ActionListener listener) {
        mListener = listener;
        return this;
    }

    // Interfaces

    interface ActionListener {
        void onSuccess();

        void onCancel();
    }
}
