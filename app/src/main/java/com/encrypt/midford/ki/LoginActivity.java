package com.encrypt.midford.ki;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.encrypt.midford.ki.LoginFragment.OnLoginFragmentInteractionListener;
import com.encrypt.midford.ki.data.ManifestRepository;
import com.encrypt.midford.ki.SetupFragment.OnSetupFragmentInteractionListener;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements OnLoginFragmentInteractionListener,
        OnSetupFragmentInteractionListener {

    // Handlers

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setupPermissions();

        ManifestRepository repository = new ManifestRepository(getApplication());

        if (repository.doesManifestExist()) {
            Fragment loginFragment = new LoginFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.replace(R.id.fragment_container, loginFragment);
            transaction.commit();
        } else {
            Fragment setupFragment = new SetupFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.replace(R.id.fragment_container, setupFragment);
            transaction.commit();
    } }

    public void onFragmentInteraction(Uri uri) { }

    // Methods

    private void setupPermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return;

        List<String> permissions = new ArrayList<>();

        if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
            permissions.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
            permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (checkSelfPermission(Manifest.permission.MANAGE_DOCUMENTS)
                != PackageManager.PERMISSION_GRANTED)
            permissions.add(android.Manifest.permission.MANAGE_DOCUMENTS);

        if (permissions.isEmpty())
            return;

        requestPermissions(permissions.toArray(new String[0]), 0);
    }
}
