package com.encrypt.midford.ki.components;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.encrypt.midford.ki.R;

import androidx.annotation.NonNull;

public class CustomDialogueBuilder {

    private CustomDialogue mDialogue;

    private CustomDialogueBuilder(Context c) {
        mDialogue = new CustomDialogue(c);
        mDialogue.create();
    }

    public static CustomDialogueBuilder fromContext(Context c) {
        return new CustomDialogueBuilder(c);
    }

    public CustomDialogueBuilder setMessage(String s) {
        mDialogue.setMessage(s);
        return this;
    }

    public CustomDialogueBuilder addContentView(View et) {
        mDialogue.addContentView(et);
        return this;
    }

    public CustomDialogueBuilder setTitle(int id) {
        mDialogue.setTitle(id);
        return this;
    }

    public CustomDialogueBuilder setIcon(int id) {
        mDialogue.setIcon(id);
        return this;
    }

    public CustomDialogueBuilder setSecondaryActionText(int id) {
        mDialogue.setSecondaryActionText(id);
        return this;
    }

    public CustomDialogueBuilder setPrimaryActionText(int id) {
        mDialogue.setPrimaryActionText(id);
        return this;
    }

    public CustomDialogueBuilder setPrimaryActionListener(@NonNull Action a) {
        mDialogue.setPrimaryAction(a);
        return this;
    }

    public CustomDialogueBuilder setDismissCondition(@NonNull DismissCondition c) {
        mDialogue.setDismissCondition(c);
        return this;
    }

    public void show() {
        mDialogue.show();
    }

    public interface Action {
        void onAction(Context context);
    }

    public interface DismissCondition {
        boolean shouldDismiss();
    }

    private class CustomDialogue extends Dialog implements View.OnClickListener {

        private Action mAction;
        private DismissCondition mCondition;

        private ImageView mIcon = findViewById(R.id.iv_icon);
        private TextView mTitle = findViewById(R.id.tv_title);
        private LinearLayout mContent = findViewById(R.id.ll_content);
        private TextView mSecondaryAction = findViewById(R.id.tv_secondary_action);
        private TextView mPrimaryAction = findViewById(R.id.tv_btn_primary_action);

        private CustomDialogue(Context c) {
            super(c);

            mCondition = new DismissCondition() {
                @Override
                public boolean shouldDismiss() {
                    return true;
                }
            };
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.custom_dialogue);

            mIcon = findViewById(R.id.iv_icon);
            mTitle = findViewById(R.id.tv_title);
            mContent = findViewById(R.id.ll_content);
            mSecondaryAction = findViewById(R.id.tv_secondary_action);
            mPrimaryAction = findViewById(R.id.tv_btn_primary_action);

            mSecondaryAction.setOnClickListener(this);
            mPrimaryAction.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_btn_primary_action:
                    if (mAction != null)
                        mAction.onAction(getContext());
                    if (mCondition != null && mCondition.shouldDismiss())
                        dismiss();
                    break;
                case R.id.tv_secondary_action:
                    dismiss();
                    break;
            }
        }

        void addContentView(View v) {
            mContent.addView(v);
        }

        public void setTitle(int id) {
            mTitle.setText(id);
        }

        void setMessage(String s) {
            TextView tv = new TextView(getContext());
            tv.setText(s);
            mContent.addView(tv);
        }

        void setIcon(int id) {
            mIcon.setImageResource(id);
            mIcon.setVisibility(View.VISIBLE);
        }

        void setSecondaryActionText(int id) {
            mSecondaryAction.setText(id);
        }

        void setPrimaryActionText(int id) {
            mPrimaryAction.setText(id);
        }

        void setPrimaryAction(@NonNull Action a) {
            mAction = a;
        }

        void setDismissCondition(@NonNull DismissCondition c) {
            mCondition = c;
        }
    }
}
